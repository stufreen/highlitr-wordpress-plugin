(function() {
  
  tinymce.create('tinymce.plugins.HighliteButton', {
    /**
     * Initializes the plugin, this will be executed after the plugin has been created.
     * This call is done before the editor instance has finished it's initialization so use the onInit event
     * of the editor instance to intercept that event.
     *
     * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
     * @param {string} url Absolute URL to where the plugin is located.
     */
    init : function(ed, url) {
			
			//Adds a new command to the tinymce object but does not add the associated button
      ed.addCommand('highliteFormat', function() {
				//First register a new type of formatting (i.e. to add "important-update" class to selected elements)
				ed.formatter.register('highliteformat', {
					selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img',
					styles : {backgroundColor : '#FFCCCC'}, /* For the visual editor. Should be over-ridden by an important rule in the css file. */
					'classes' : 'important-update'
				});
				//Then specify that each time the command is called the format should be toggled
        ed.formatter.toggle('highliteformat');
      });
      
			//Add the button to the editor and associate it with the new command created above
      ed.addButton('highlite', {
        title : 'Highlite',
        cmd : 'highliteFormat',
        image : url + '/pre.png'
      });
    },
  });

  // Register plugin
  tinymce.PluginManager.add('highliteButton', tinymce.plugins.HighliteButton);

})();