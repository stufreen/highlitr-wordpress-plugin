<?php
/**
 * Plugin Name: Highlitr
 * Description: This plugin adds a button to 
 * the visual editor to wrap some text in an 
 * 'important-update' class. Based on tinymce-pre 
 * by mortalis.
 * Version: 0.1
 * Author: Stu Freen
 * Author URI: http://stufreen.com
 * License: MIT
 */

// buttons param is passed in by TinyMCE
function mcehighlitr_register_buttons($buttons) {
  array_push( $buttons, 'highlite');
  return $buttons;
}

// plugin_array param is passed in by TinyMCE
function mcehighlitr_register_tinymce_javascript($plugin_array) {
  $plugin_array['highliteButton'] = plugins_url('mce-highlitr',__FILE__) . '/plugin.js';
  return $plugin_array;
}

function mcehighlitr_buttons() {
  add_filter('mce_buttons', 'mcehighlitr_register_buttons'); // Add new button. 'mce_buttons' is a special hook for TinyMCE
	add_filter("mce_external_plugins", "mcehighlitr_register_tinymce_javascript"); // Load the highlitr plugin. 'mce_external_plugins' is a special hook for TinyMCE
}
add_action( 'init', 'mcehighlitr_buttons' ); //'init' is a Wordpress hook